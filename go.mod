module github.com/mdouchement/fichenn

go 1.18

require (
	filippo.io/age v1.0.0
	github.com/atotto/clipboard v0.1.4
	github.com/fxamacker/cbor/v2 v2.4.0
	github.com/gofrs/uuid v4.2.0+incompatible
	github.com/k0kubun/go-ansi v0.0.0-20180517002512-3bf9e2903213
	github.com/klauspost/compress v1.15.7
	github.com/knadh/koanf v1.4.2
	github.com/mdouchement/logger v0.0.0-20200719134033-63d0eda5567d
	github.com/pkg/errors v0.9.1
	github.com/rs/cors v1.8.2
	github.com/schollz/progressbar/v3 v3.8.6
	github.com/sirupsen/logrus v1.8.1
	github.com/spf13/cobra v1.5.0
	github.com/vugu/vgrouter v0.0.0-20200725205318-eeb478c42e5d
	github.com/vugu/vjson v0.0.0-20200505061711-f9cbed27d3d9
	github.com/vugu/vugu v0.3.4
	gopkg.in/yaml.v3 v3.0.0
)

require (
	github.com/fsnotify/fsnotify v1.5.4 // indirect
	github.com/inconshreveable/mousetrap v1.0.0 // indirect
	github.com/mattn/go-colorable v0.1.12 // indirect
	github.com/mattn/go-isatty v0.0.14 // indirect
	github.com/mattn/go-runewidth v0.0.13 // indirect
	github.com/mgutz/ansi v0.0.0-20200706080929-d51e80ef957d // indirect
	github.com/mitchellh/colorstring v0.0.0-20190213212951-d06e56a500db // indirect
	github.com/mitchellh/copystructure v1.2.0 // indirect
	github.com/mitchellh/mapstructure v1.5.0 // indirect
	github.com/mitchellh/reflectwalk v1.0.2 // indirect
	github.com/pelletier/go-toml v1.9.5 // indirect
	github.com/rivo/uniseg v0.2.0 // indirect
	github.com/spf13/pflag v1.0.5 // indirect
	github.com/vugu/xxhash v0.0.0-20191111030615-ed24d0179019 // indirect
	github.com/x448/float16 v0.8.4 // indirect
	golang.org/x/crypto v0.0.0-20220622213112-05595931fe9d // indirect
	golang.org/x/sys v0.0.0-20220708085239-5a0f0661e09d // indirect
	golang.org/x/term v0.0.0-20220526004731-065cf7ba2467 // indirect
)
